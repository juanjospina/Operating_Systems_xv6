#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/file.h>
#include <sys/mman.h>
#include <string.h>

#define PIECE 4096 // 4 KB piece of Data

int main(int argc, char *argv[])
{

	// Checks that the user entered the 2 arguments.	
	if(argc < 3)
	{
		fprintf( stderr, "missing or bad command line arguments\n" );
		exit( -1 );
	}

	// Files declarations
	char buf[PIECE];
	FILE* inputF;
	FILE* outputF;
	size_t nread;

	// Shared Memory 
	const char *memname = "memory"; // name of shared memory
	const int regionSize = PIECE;  // COULD Be size_t

	// Opening files 
	inputF = fopen(argv[1], "r");
	outputF = fopen(argv[2], "w");

	// Process pid and Pipes
	pid_t pid;									// process id
	int parent_child_pipefd[2];	// parent to child pipe 
	int child_parent_pipefd[2]; // child to parent pipe

	int ret1;
	int ret2;

	ret1 = pipe(parent_child_pipefd);
	ret2 = pipe(child_parent_pipefd);


	// Check pipe1 was created correctly
	if(ret1 == -1)
	{
		perror("parent pipe");
		exit(-1);
	}

	// Check pipe2 was created correctly
	if(ret2 == -1)
	{
		perror("child pipe");
		exit(-1);
	}

	// Process creation
	pid = fork();

	if(pid == 0)
	{
		// Child process
		int block_num = 1;
		int leido1 = 0;
		char *shm_base;
		char *ptr;
		// Create the shared memory
		int return_shmfd = shm_open(memname, O_RDONLY, 0666);

		// Checks that shm_open worked correctly
		if(return_shmfd == -1)
		{
			perror("shm_open");
			exit(EXIT_FAILURE);
		}

		// Closing Pipes not used.
		close(parent_child_pipefd[1]);
		close(child_parent_pipefd[0]);

		shm_base = mmap(0, regionSize, PROT_READ, MAP_SHARED, return_shmfd, 0);

		if (shm_base == MAP_FAILED)
		{
			perror("mmap-CHILD");
			exit(EXIT_FAILURE);
		}

		ptr = shm_base;

		while(block_num !=0)
		{	
			leido1 = read(parent_child_pipefd[0], &block_num, sizeof(block_num));

			if(block_num == 0)
				break;

			if(leido1  > 0)
			{
				//	ptr += sprintf(temp,"%s",ptr);					// Put the strings on the memory.
				fwrite(ptr,1, regionSize/*sizeof(ptr)*/,outputF); 
				printf("%d\n",block_num);
				write(child_parent_pipefd[1], &block_num, sizeof(block_num));
				leido1 = 0;
			}
		}

		// remove the mapped shared memory segment from the address space of the process 
		if (munmap(shm_base, regionSize) == -1) 
		{
			perror("munmap child");
			exit(EXIT_FAILURE);
		}

		// close the shared memory segment as if it was a file 
		if (close(return_shmfd) == -1) 
		{
			perror("closing-shm_fd-child");
			exit(EXIT_FAILURE);
		}

		// remove the shared memory segment from the file system 
		if (shm_unlink(memname) == -1) 
		{
			perror("Unliking-child");
			exit(EXIT_FAILURE);
		}


	}else
	{
		// Parent process
		//int returnStatus;

		// Create the shared memory
		int returnFtrunc;		// return of ftruncate()
		int return_shmfd = shm_open(memname, O_CREAT | O_RDWR, 0666);

		int block_num = 1;			// block number 

		char *shm_base; // base address
		char *ptr;			// movable address

		// Checks that shm_open worked correctly
		if(return_shmfd == -1)
		{
			perror("shm_open");
			exit(EXIT_FAILURE);
		}

		returnFtrunc = ftruncate(return_shmfd, regionSize);

		// Checks ftruncate worked correctly
		if (returnFtrunc != 0)
		{
			perror("ftruncate");
			exit(EXIT_FAILURE);
		}

		shm_base = mmap(0, regionSize, PROT_READ | PROT_WRITE, MAP_SHARED, return_shmfd, 0);

		if (shm_base == MAP_FAILED)
		{
			perror("mmap");
			exit(EXIT_FAILURE);
		}

		// Closing Pipes not used.
		close(parent_child_pipefd[0]);
		close(child_parent_pipefd[1]);

		ptr = shm_base;	// Initialize address
		int leido = 0;
		int confirmation = 0;

		// Write data from input file to share data 
		if(inputF)
		{
			while((nread = fread(buf, 1, sizeof buf, inputF)) > 0 )
			{
				printf("pasee PARENT\n");

				//printf("%s\n",ptr);

				sprintf(ptr,"%s",buf);					// Put the strings on the memory.
				write(parent_child_pipefd[1], &block_num, sizeof(block_num));// Send block number
				//block_length = strlen(ptr);																	
				//write(parent_child_pipefd[1], &block_length, sizeof(block_length));
				while((leido=read(child_parent_pipefd[0], &confirmation, sizeof(confirmation)))>0)
				{
					printf("confirmation = %d\n", confirmation);
					printf("block_num = %d\n", block_num);

					if(confirmation == block_num)
						break;
				}

				block_num = block_num + 1;
//				sprintf(ptr,"%s","\0");
//				sprintf(buf,"%s","\0");	

				//memset(&buf,0,sizeof(buf));

			}		

			printf("YESS\n");
			// Finally to end everything send block num = 0 block lenght = 0
			int terminated = 0;
			write(parent_child_pipefd[1], &terminated, sizeof(terminated));

		}

		// Remove the mapped memory segment from the address space of the process
		if (munmap(shm_base, regionSize) == -1)
		{
			perror("munmap parent");
			exit(EXIT_FAILURE);
		}

		// Close the shared memory segment as if it was a file */
		if (close(return_shmfd) == -1) 
		{
			perror("closing-shared-memory-parent");
			exit(EXIT_FAILURE);
		}

		//	waitpid(pid, &returnStatus, 0); // waits for child process to finish.
		fclose(inputF);
		fclose(outputF);

	}

	return 0;

}



