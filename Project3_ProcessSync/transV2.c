#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

#define PIECE 4096 // 4 KB piece of Data

int main(int argc, char *argv[])
{

	// Checks that the user entered the 2 arguments.	
	if(argc < 3)
	{
		fprintf( stderr, "missing or bad command line arguments\n" );
		exit( -1 );

	}

	char buf[PIECE];
	FILE* inputF;
	FILE* outputF;
	size_t nread;

	inputF = fopen(argv[1], "r");
	outputF = fopen(argv[2], "w");

	pid_t pid;									// process id
	int parent_child_pipefd[2];	// parent to child pipe 
	int child_parent_pipefd[2]; // child to parent pipe

	int ret1;
	int ret2;

	ret1 = pipe(parent_child_pipefd);
	ret2 = pipe(child_parent_pipefd);


	// Check pipe1 was created correctly
	if(ret1 == -1)
	{
		perror("parent pipe");
		exit(-1);
	}

	// Check pipe2 was created correctly
	if(ret2 == -1)
	{
		perror("child pipe");
		exit(-1);
	}


	// Process creation
	pid = fork();

	if(pid == 0)
	{
		// Child process

	}else
	{
		// Parent process
		int returnStatus;

		if(inputF)
		{
			while((nread = fread(buf, 1, sizeof buf, inputF)) > 0 ) 
				fwrite(buf, 1, nread, stdout);

			fclose(inputF);
			fclose(outputF);
		}

		waitpid(pid, %returnStatus, 0); // waits for child process to finish.

	}

	return 0;

}



/*

	 static inline int readline( FILE *fp, char *buf, int max, int *eof )
	 {
	 int len = 0;
	 char c;

// get next entry 
while ( len < max ) {
c = fgetc( fp );
if ( c == EOF ) {
 *eof = 1;
 break;
 }
 buf[len++] = c;
 if ( c == '\n' )
 break;
 }      

 return len;
 }

 */
