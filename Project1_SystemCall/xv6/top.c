#include "types.h"
#include "user.h"
#include "uproc.h"

// Maximum process showed
#define MAX 50

/*
Author:       Juan Jose Ospina
Date:         02/07/2016
Description:  Top System Call Implementation. Max 50 processes
*/

int main(int argc, char *argv[])
{  
  
	int check = 0;
	struct uproc proctable[MAX];
  struct uproc* process; 
  
	// BUbble Sort Variables
  struct uproc temp;
	struct uproc* count;
 	int j = 0;
	int i = 0;
	int counter = 0;
  count = &proctable[0];
	//-----------------------
  
	check = getprocs(50*sizeof(struct uproc),&proctable);
  
	if( check != 0)
    printf(1,"\nError Process Table\n");
  
  process = &proctable[0];
 	
	// Bubble Sort
	while(count != &proctable[MAX-1] && count->state != 0)
	{
		counter++;
		count++;
	}

	for(i = 0; i < counter-1; i++)
	{
		
		for (j = 0; j < counter-1-i; j++)
		{
			if(proctable[j].sz < proctable[j+1].sz)
			{
				temp = proctable[j];
				proctable[j] = proctable[j+1]; 
				proctable[j+1] = temp;
				
			}else if(proctable[j].sz == proctable[j+1].sz)
			{
				if(strcmp(proctable[j].name,proctable[j+1].name)>0)
				{
					temp = proctable[j];
					proctable[j] = proctable[j+1]; 
					proctable[j+1] = temp;

				}	
			}

		}

	}

  
	printf(1, "\nPID  STATE  SIZE  NAME\n");
  
	while(process != &proctable[MAX-1] && process->state != 0)
	{
 
	  printf(1,"%d  ",process->pid);

		if(process->state == 0)
      printf(1,"%s  ", "UNUSED");
		else if(process->state == 1)
      printf(1,"%s  ", "EMBRYO");
		else if(process->state == 2)
			printf(1,"%s  ", "SLEEPING");
		else if(process->state == 3)
			printf(1,"%s  ", "RUNNABLE");
		else if(process->state == 4)
 			printf(1,"%s  ", "RUNNING");
 		else if(process->state == 5)
			printf(1,"%s  ", "ZOMBIE");
		else
			printf(1,"%s  ", "TERMINATED");

		printf(1, "%d  ", process->sz); 

    printf(1,"%s\n", process->name);

    process++;
  }
     
  exit();
}




