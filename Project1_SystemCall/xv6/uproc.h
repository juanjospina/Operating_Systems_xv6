/*
Author:       Juan Jose Ospina
Date:         02/07/2016
Description: UPROC structure defined
*/

struct uproc 
{
	int pid;
	int state;
	uint sz;
	char name[16];
};
