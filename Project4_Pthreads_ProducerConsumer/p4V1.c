#include <stdio.h>
#include <stdlib.h>
#include <semaphore.h>
#include <pthread.h>

typedef int buffer_item;
#define BUFFER_SIZE 5

// Buffer with size = BUFFER_SIZE
buffer_item buffer[BUFFER_SIZE];

// Threads Data 
pthread_t tid;				// thread id
pthread_attr_t attr;	// thread attr

// Semaphores  
sem_t empty;
sem_t full;

// Mutex Lock
pthread_mutex_t mutex;

void *producer(void *param);
void *consumer(void *param);
void init();

int counter;	// Counter indicating position in buffer.

// Main Loop
int main(int argc, char *argv[])
{
	
	int temp[3];
	int c1;

	// Error checking of arguments 
	if(argc < 4 || argc > 4)
	{
		fprintf(stderr,"Enter 3 arguments : (sleep time) - (number of producer threads) - (number of consumer threads)\n");
		return -1;
	}

	// Convert char arguments to integers
	temp[0] = atoi(argv[1]); // sleep time
	temp[1] = atoi(argv[2]); // number of producers
	temp[2] = atoi(argv[3]); // number of consumers

	// Check sleep time is greater or equal 0
	if(temp[0] < 0)
	{
		fprintf(stderr, "sleep time must be greater or equal to 0\n");
		return -1;
	}
	// Check that number of producers is greater than 0
	if(temp[1] < 1)
	{
		fprintf(stderr, "number of producers must be greater than 0\n");
		return -1;
	}
	if(temp[2] < 1)
	{
		fprintf(stderr, "number of consumers must be greater than 0\n");
		return -1;
	}    

	init();

	/* Do actual work from this point forward */
	/* Create the producer threads */
	for(c1=1; c1<=temp[1]; c1++)
	{
		pthread_create(&tid, &attr, producer, NULL);
		printf("Creating producer #%d\n", c1);    
	}

	/* Create the consumer threads */
	for(c1=1; c1<=temp[2]; c1++)
	{
		pthread_create(&tid, &attr, consumer, NULL);
		printf("Creating consumer #%d\n", c1);    
	}

	/* Ending it */
	sleep(temp[0]);

	printf("Production complete.\n");
	exit(0);
}

void init()
{
	int c2;

	pthread_mutex_init(&mutex, NULL); /* Initialize mutex lock */
	pthread_attr_init(&attr); /* Initialize pthread attributes to default */
	sem_init(&full, 0, 0); /* Initialize full semaphore */
	sem_init(&empty, 0, BUFFER_SIZE); /* Initialize empty semaphore */
	counter = 0; /* Initialize global counter */ 
	for(c2=0;c2<BUFFER_SIZE;c2++) /* Initialize buffer */
	{
		buffer[c2] = 0;
	}
}

void *producer(void *param)
{
	/* Variables */
	buffer_item item;

	while(1)
	{ 
	//	sleep(rand());  
		sleep(1);
		item = (rand()); /* Generates random item */ 

		sem_wait(&empty); /* Lock empty semaphore if not zero */
		pthread_mutex_lock(&mutex);

		if(insert_item(item))
		{
			fprintf(stderr, "Producer error."); 
		}
		else
		{
			printf("Producer produced %d\n", item); 
		}

		pthread_mutex_unlock(&mutex);
		sem_post(&full); /* Increment semaphore for # of full */
	}
}

void *consumer(void *param)
{
	buffer_item item;

	while(1)
	{
		//sleep(rand());
		sleep(1);
		sem_wait(&full); /* Lock empty semaphore if not zero */
		pthread_mutex_lock(&mutex);
		if(remove_item(&item))
		{
			fprintf(stderr, "Consumer error."); 
		}
		else
		{
			printf("Consumer consumed %d\n", item);
		}

		pthread_mutex_unlock(&mutex);
		sem_post(&empty); /* Increments semaphore for # of empty */
	}
}

int insert_item(buffer_item item)
{
	if(counter < BUFFER_SIZE) /* Buffer has space */
	{
		buffer[counter] = item;
		counter++;
		return 0;
	}
	else /* Buffer full */
	{
		return -1;
	}
}

int remove_item(buffer_item *item)
{
	if(counter > 0) /* Buffer has something in it */
	{
		*item = buffer[(counter-1)];
		counter--;
		return 0;
	}
	else /* Buffer empty */
	{
		return -1;
	}
}
