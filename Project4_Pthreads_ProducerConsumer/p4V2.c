#include <stdio.h>
#include <stdlib.h>
#include <semaphore.h>
#include <pthread.h>

// Definition of buffer type
typedef int buffer_item;
#define BUFFER_SIZE 5

// Buffer with size = BUFFER_SIZE
buffer_item buffer[BUFFER_SIZE];

// Threads Data 
pthread_t tid;				// thread id
pthread_attr_t attr;	// thread attr

// Semaphores  
sem_t empty;
sem_t full;

// Mutex Lock
pthread_mutex_t mutex;

// Prototypes for Producer and Consumer functions
void *producer(void *param);
void *consumer(void *param);
// Prototypes for Insert and Remove functions
int insert_item(buffer_item item);
int remove_item(buffer_item *item);

int counter;	// Counter indicating position in buffer.

// Main Loop
int main(int argc, char *argv[])
{
	
	int sleep_time;
	int number_producers;
	int number_consumers;

	// Error checking of arguments 
	if(argc < 4 || argc > 4)
	{
		fprintf(stderr,"Enter 3 arguments : (sleep time) - (number of producer threads) - (number of consumer threads)\n");
		return -1;
	}

	// Convert char arguments to integers
	sleep_time = atoi(argv[1]); // sleep time
	number_producers = atoi(argv[2]); // number of producers
	number_consumers = atoi(argv[3]); // number of consumers

	// Check sleep time is greater or equal 0
	if(sleep_time < 0)
	{
		fprintf(stderr, "sleep time must be greater or equal to 0\n");
		return -1;
	}
	// Check that number of producers is greater than 0
	if(number_producers < 1)
	{
		fprintf(stderr, "number of producers must be greater than 0\n");
		return -1;
	}
	// Check that number of consumers is greater than 0
	if(number_consumers < 1)
	{
		fprintf(stderr, "number of consumers must be greater than 0\n");
		return -1;
	}    


	// Thread Initialization 
	pthread_mutex_init(&mutex, NULL); 
	pthread_attr_init(&attr);
	// Semaphore Initialization
	sem_init(&empty, 0, BUFFER_SIZE);
	sem_init(&full, 0, 0); 

	counter = 0;  

	// Initialize with zeros the buffer
	int i;
	for(i = 0; i < BUFFER_SIZE; i++) 
		buffer[i] = 0;

	int j;
	// Producer threads
	for(j = 1; j <= number_producers; j++)
	{
		pthread_create(&tid, &attr, producer, NULL);
	}

	// Consumer threads 
	for(j = 1; j <= number_consumers; j++)
	{
		pthread_create(&tid, &attr, consumer, NULL);
	}

	// Sleeping time
	sleep(sleep_time);

	// Exit
	printf("Terminating\n");
	exit(0);
}


// Insert item to Buffer
int insert_item(buffer_item item)
{
	if(counter < BUFFER_SIZE) 
	{
		// inserting item to buffer
		buffer[counter] = item;
		counter++;
		return 0;
	}
	else // full buffer
		return -1;
}

// Remove item from Buffer
int remove_item(buffer_item *item)
{
	// Checking buffer is not empty
	if(counter > 0) 
	{
		// "remove" item from buffer
		*item = buffer[(counter-1)];
		counter--;
		return 0;
	}
	else // buffer is empty
		return -1;
}



void *producer(void *param)
{
	
	buffer_item item;

	while(1)
	{ 
		sleep(1);

		// Generates a random number from up to 100
		item = (rand()%100)+1;
		
		sem_wait(&empty);							// waiting/locking for empty semaphore 
		pthread_mutex_lock(&mutex);		// locks the mutex semaphore

		if(insert_item(item))
			fprintf(stderr, "Error while producing"); 
		else
			printf("Producer produced %d\n", item); 

		pthread_mutex_unlock(&mutex);	// unlocks mutex semaphore
		sem_post(&full);							// release
	}
}


void *consumer(void *param)
{

	buffer_item item;

	while(1)
	{
		sleep(1);

		sem_wait(&full);							// wait/locking for full semaphore
		pthread_mutex_lock(&mutex);		// locks mutex semaphore
		
		if(remove_item(&item))
			fprintf(stderr, "Error while consuming"); 
		else
			printf("Consumer consumed %d\n", item);

		pthread_mutex_unlock(&mutex);	// unlocks mutex sempahore
		sem_post(&empty);							// release
	}
}


